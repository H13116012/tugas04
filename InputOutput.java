package tugas04;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class InputOutput {
	static Scanner input=new Scanner(System.in);
	void copyPaste() throws IOException{
		FileInputStream in = null;
		FileOutputStream out = null;
		try {
						
			System.out.println("tentukan lokasi file .txt yang akan anda copy");
			System.out.println("contoh: ('D:/java/test/')");
			System.out.println("jika file .txt berada pada folder yang sama, ketik 'default'");
			String inSource=input.next();
			String outSource;
			if (inSource.contentEquals("default")){
				inSource= "";
			}
			System.out.println("tentukan nama file .txt");
			System.out.println("contoh: 'tugas.txt'");
			String filename=input.next();
			do{
				System.out.println("tentukan lokasi dimana akan anda paste");
				System.out.println("contoh: ('E:/tugas/')");
				outSource=input.next();
				if(outSource.contentEquals(inSource)){
				System.out.println("silahkan pilih tempat yang berbeda");
				continue;
			}
			break;
			}while(true);
				in = new FileInputStream(inSource+filename);
				out = new FileOutputStream(outSource+filename);
				int c;
				while ((c = in.read()) != -1) {		//-1 means end of file
					out.write(c);
				}
				System.out.println(filename+" berhasil copy ke "+outSource);
				}catch(FileNotFoundException e){
				System.out.println("file tidak terdeteksi");
				}finally {
				if (in != null) {
				in.close();
				}
				if (out != null) {
				out.close();
				}
			}
			input.nextLine();
		}
		void cutPaste() throws IOException{
			FileInputStream in = null;
			FileOutputStream out = null;
			try {
				System.out.println("tentukan lokasi file .txt yang akan anda cut");
				System.out.println("contoh: ('C:/java/test/')");
				System.out.println("jika file .txt berada pada folder yang sama, ketik 'default'");
				String inSource=input.next();
				String outSource;
				if (inSource.contentEquals("default")){
					inSource= "";
				}
				System.out.println("tentukan nama file .txt");
				System.out.println("example: 'tugas.txt'");
				String filename=input.next();
				File source = new File(inSource+filename);
				do{
				System.out.println("tentukan lokasi dimana akan anda paste");
				System.out.println("example: ('E:/tugas/')");
					outSource=input.next();
				if(outSource.contentEquals(inSource)){
				System.out.println("silahkan pilih tempat yang berbeda");
				continue;
				}
				break;
				}while(true);
				in = new FileInputStream(inSource+filename);
				out = new FileOutputStream(outSource+filename);
				int c;
				while ((c = in.read()) != -1) {		//-1 means end of file
					out.write(c);
				}
				source.deleteOnExit();
				System.out.println(filename+" berhasil cut ke "+outSource);
				}catch(FileNotFoundException e){
				System.out.println("file tidak terdeteksi");
				}finally {
				if (in != null) {
				in.close();
				}
				if (out != null) {
				out.close();
				}
			}
			input.nextLine();
		}
		void permutation() throws IOException{
			MainMenu();
		}
		static void MainMenu() throws IOException{
			int select =-1;
			do{
				System.out.println("Menu :");
				System.out.println("1. Permutasi");
				System.out.println("2. Combinasi");
				System.out.println("0. keluar");
				do{
					try{
						System.out.print("masukkan pilihan anda : ");
						select=input.nextInt();
						break;
						}catch(InputMismatchException e){
							System.out.println("pilihan salah, silahkan masukkan kembali");
							input.nextLine();
						}
				}while(true);
				switch(select){
				case 1:
					permutasi();
					break;
				case 2:
					kombinasi();
					break;
				case 0:
					break;
				}
			}while(select!=0);
		}	
		static void permutasi() throws IOException{
			FileInputStream in = null;
			try{
				in = new FileInputStream("variable.txt");
				System.out.println("-------------------------");
				System.out.println("Permutasi");
				int N=0,R=0,NR;
				
				String string="";
				int reader;
				
				do{
					reader=(char)in.read();
					
					if((reader>57 && reader !=32 && reader !=-1) || (reader<48 && reader !=32 && reader !=-1)){
						throw new IOException();
					}
					if (reader ==' ')break;
					string=string+String.valueOf((char)reader);
				}while(true);
			
				N =Integer.valueOf(string);
				System.out.println("nilai N : "+N);
				string="";
				
				
				while((reader=in.read())!=-1){
					string=string+String.valueOf((char)reader);
					if((reader>57 && reader !=32 && reader !=-1) || (reader<48 && reader !=32 && reader !=-1)){
						throw new IOException();
					}
					reader=(char)in.read();			
				}
				
				R =Integer.valueOf(string);
				System.out.println("nilai R : "+R);
				if(N<R){
					throw new Exception();
				}
				int Ntotal=1;
				int NRtotal=1;
				
				
				NR=N-R;
				for(int index =1;index<=N;index++){
					Ntotal*=index;
				}
				for(int index =1;index<=NR;index++){
					NRtotal*=index;
				}
				System.out.println("Permutasi nilai N and R : "+(Ntotal/NRtotal));
				System.out.println("-------------------------");
			}catch(FileNotFoundException e){
				System.out.println("\nfile tidak terdeteksi");
				System.out.println("program ini membutuhkan variabel .txt (dengan 2 variable didalamnya) lalu run");
				System.out.println("-------------------------");
			}catch(IOException e){
				System.out.println("Error.");
				System.out.println("variable.txt tidak memiliki 2 nilai (dipisahkan oleh spasi)");
				System.out.println("-------------------------");
			}catch(NumberFormatException e){
				System.out.println("Error.");
				System.out.println("variable.txt tidak memiliki 2 nilai (dipisahkan oleh spasi)");
				System.out.println("-------------------------");
			}catch(Exception e){
				System.out.println("nilai N adalah kurang dari nilai R");
				System.out.println("silahkan ubah nilai variabel .txt");
				System.out.println("-------------------------");
			}finally{
				if (in != null) {
					in.close();
					}
			}
		}
		static void kombinasi() throws IOException{
			FileInputStream in = null;
			try{
				in = new FileInputStream("variable.txt");
				System.out.println("-------------------------");
			System.out.println("Combinasi");
			int N,R,NR;
			String string="";
			int reader;
			
			do{
				reader=(char)in.read();
				if((reader>57 && reader !=32 && reader !=-1) || (reader<48 && reader !=32 && reader !=-1)){
					throw new IOException();
				}
				if (reader ==' ')break;
				string=string+String.valueOf((char)reader);
			}while(true);
		
			N =Integer.valueOf(string);
			System.out.println("nilai N : "+N);
			string="";
			
			
			while((reader=in.read())!=-1){
				string=string+String.valueOf((char)reader);
				if((reader>57 && reader !=32 && reader !=-1) || (reader<48 && reader !=32 && reader !=-1)){
					throw new IOException();
				}
				reader=(char)in.read();
				
				
			}
			
			R =Integer.valueOf(string);
			System.out.println("nilai R : "+R);
			if(N<R){
				throw new Exception();
			}
			int Ntotal=1;
			int Rtotal=1;
			int NRtotal=1;
			
			NR=N-R;
			for(int index =1;index<=N;index++){
				Ntotal*=index;
			}
			for(int index =1;index<=R;index++){
				Rtotal*=index;
			}
			for(int index =1;index<=NR;index++){
				NRtotal*=index;
			}
			System.out.println("Combinasi nilai N and R : "+(Ntotal/(Rtotal*NRtotal)));
			System.out.println("-------------------------");
			}catch(FileNotFoundException e){
				System.out.println("\nfile tidak terdeteksi");
				System.out.println("program ini membutuhkan variable.txt (dengan 2 variable didalamnya) lalu run");
				System.out.println("-------------------------");
			}catch(IOException e){
				System.out.println("Error.");
				System.out.println("variable.txt tidak memiliki 2 nilai (dipisahkan oleh spasi)");
				System.out.println("-------------------------");
			}catch(NumberFormatException e){
				System.out.println("Error.");
				System.out.println("variable.txt tidak memiliki 2 nilai (dipisahkan oleh spasi)");
				System.out.println("-------------------------");
			}catch(Exception e){
				System.out.println("nilai N adalah kurang dari nilai R");
				System.out.println("silahkan ubah nilai variabel .txt");
				System.out.println("-------------------------");
			}finally{
				if (in != null) {
					in.close();
					}
			}
		}
		
		void pascal() throws IOException{
			FileInputStream in = null;
			FileOutputStream out = null;
			
			try{
				in = new FileInputStream("pascalInput.txt");
				out = new FileOutputStream("pascalOutput.txt");
				int rows;
				int reader;
				String string="";
				while((reader=in.read()) !=-1){
					if((reader>57 && reader !=32 && reader !=-1) || (reader<48 && reader !=32 && reader !=-1)){
						throw new IOException();
					}
					string=string+String.valueOf((char)reader);
					
					if((reader=in.read()) !=-1){
						
						string=string+String.valueOf((char)reader);
					}
					
				}
				rows =Integer.valueOf(string);
				
				String newLine = System.getProperty("line.separator");
				
				int c_row,c_number;			// c_row represents the current row to print(starting from index 0)
											// c_number represents the current sequence of number to print(starting from 1)
	      
		        for (c_row = 0; c_row < rows; c_row++) {				//number of rows to print
		        	
			            int spaces =(rows-c_row);					//number of spaces to print
			          
			            for (int x=0; x<spaces;x++){//printing spaces before the first number on each row
			            	string="   ";
			            	for(int index=0;index<string.length();index++){
			    	        	out.write(string.charAt(index));
			    	        }	
			    		}		              
			            long number=1;								//starting from number 1		            
			            for (c_number = 0; c_number <= c_row; c_number++){	                
			                if(number<10){
			                	string="  "+number+"   ";
			                	for(int index=0;index<string.length();index++){
				    	        	out.write(string.charAt(index));
				    	        }
			                		 	 //printing 5 spaces for 1 digit number
			                }else if(number<100){
			                	string=("  "+number+"  "); 		 //printing 4 spaces for 2 digits number
			                	for(int index=0;index<string.length();index++){
				    	        	out.write(string.charAt(index));
				    	        }
			                }else if(number<1000){
			                	string=(" "+number+"  ");  		 //printing 3 spaces for 3 digits number
			                	for(int index=0;index<string.length();index++){
				    	        	out.write(string.charAt(index));
				    	        }
			                }else{
			                	string=(" "+number+" ");  		 	 //printing 2 spaces for 4 digits or more number
			                	for(int index=0;index<string.length();index++){
				    	        	out.write(string.charAt(index));
				    	        }
			                }
			                number=number*(c_row - c_number) / (c_number + 1);			 //the pascal'string pattern
			            }
			            string=newLine;
			            for(int index=0;index<string.length();index++){
		    	        	out.write(string.charAt(index));
		    	        }
		       }
			}catch(FileNotFoundException e){
				System.out.println("\nfile tidak terdeteksi");
				System.out.println("program pascalInput membutuhkan .txt (dengan nilai 'baris' variable) lalu run");
				System.out.println("-------------------------");
			}catch(IOException e){
				System.out.println("Error.");
				System.out.println("The pascalInput.txt tidak membutuhkan angka");
				System.out.println("silahkan ubah nilai pascalInput.txt");
				System.out.println("-------------------------");
			}catch(NumberFormatException e){
				System.out.println("Error.");
				System.out.println("The pascalInput.txt tidak membutuhkan angka");
				System.out.println("silahkan ubah nilai pascalInput.txt");
				System.out.println("-------------------------");
			}finally{
				if (in != null) {
					in.close();
				}
				if (out != null) {
					out.close();
				}
			}
		}
		

		void parabola() throws IOException{
			FileInputStream in = null;
			try{
				in = new FileInputStream("parabola.txt");
				System.out.println("-------------------------");
				System.out.println("Parabola");
				int a=0,b=0,c=0;
				
				String string="";
				int reader;
				boolean negative=false;
					
				do{
					reader=(char)in.read();
					if (reader==45)negative=true;		
					else if((reader>57 && reader !=32 && reader !=-1) || (reader<48 && reader !=32 && reader !=-1)){
						throw new IOException();
					}
					if (reader ==' ')break;
					if(reader!=45)string=string+String.valueOf((char)reader);
				}while(true);
			
				a =Integer.valueOf(string);
				if(negative)a=-a;
				negative=false;
				

				string="";
				
				
				
				do{
					reader=(char)in.read();
					if (reader==45)negative=true;		
					else if((reader>57 && reader !=32 && reader !=-1) || (reader<48 && reader !=32 && reader !=-1)){
						throw new IOException();
					}
					if (reader ==' ')break;
					if(reader!=45)string=string+String.valueOf((char)reader);
				}while(true);
			
				b =Integer.valueOf(string);
				if(negative)b=-b;
				negative=false;
				
			
				string="";
				
				if ((reader=in.read())==(-1))System.out.println("end of file");
				
				while(true){
					
					
					if (reader==45)negative=true;		
					else if((reader>57 && reader !=32 && reader !=-1) || (reader<48 && reader !=32 && reader !=-1)){
						throw new IOException();
					}
					if(reader!=45)string=string+String.valueOf((char)reader);
					if ((reader=in.read())==-1)break;
					else continue;
					
				}
				
				c =Integer.valueOf(string);
				if(negative)c=-c;
				negative=false;
				
				
				int D=(b*b) - (4*a*c);
				int titikBalikX=(-b/(2*a));
				int titikBalikY= D / (-4*a);
				System.out.println(a+"x^2 + "+b+"x + "+c);
				System.out.println("titik balik ("+titikBalikX+","+titikBalikY+")");
				
				if (a>0){
					System.out.println("Grafik Terbuka Ke Atas, Titik Balik Minimum");
				}
				else if (a<0){
					System.out.println("Grafik Terbuka Ke Bawah, Titik Balik Maksimum");
				}
				
				if (titikBalikX < 0) System.out.println("Titik Balik Terletak Di kiri Sumbu Y");
				else if (titikBalikX > 0) System.out.println("Titik Balik Terletak Di kanan Sumbu Y");
				else System.out.println("Titik Balik Terletak Di Sumbu Y");
				
				if (c < 0) System.out.println("Grafik Memotong Sumbu Y Di bawah Sumbu X");
				else if (c > 0) System.out.println("Grafik Memotong Sumbu Y Di atas Sumbu X");
				else System.out.println("Grafik Memotong Sumbu Y Sumbu X");
				
				
				
				System.out.println("-------------------------");
			}catch(FileNotFoundException e){
				System.out.println("\nfile tidak terdeteksi");
				System.out.println("program ini membutuhkan parabola.txt (dengan 3 variable didalamnya) lalu run");
				System.out.println("-------------------------");
			}catch(IOException e){
				System.out.println("Error.");
				System.out.println("parabola.txt tidak memiliki 3 nilai (dipisahkan oleh spasi)");
				System.out.println("-------------------------");
			}catch(NumberFormatException e){
				System.out.println("Error.");
				System.out.println("parabola.txt tidak memiliki 3 nilai (dipisahkan oleh spasi)");
				System.out.println("-------------------------");
			}finally{
				if (in != null) {
					in.close();
					}
			}
		}

		
		public static void main(String args[]) throws IOException {
			do{
				InputOutput run = new InputOutput();
				System.out.println("Menu");
				System.out.println("1. Copy-paste");
				System.out.println("2. Cut-paste");
				System.out.println("3. Permutasi-Combinasi");
				System.out.println("4. Segitiga Pascal");
				System.out.println("5. Parabola");
				System.out.print("masukkan pilihan anda : ");
				String choice = input.nextLine();
						
				switch(choice){
				case "1":
					run.copyPaste();
					break;
				case "2":
					run.cutPaste();
					break;
				case "3":
					run.permutation();
					break;
				case "4":
					run.pascal();
					break;
				case "5":
					run.parabola();
					break;
					
				default:
					System.out.println("pilihan tidak sesuai");
					continue;
					
				}
				break;
			}while(true);
			
		}
	}
